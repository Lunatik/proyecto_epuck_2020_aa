import b0RemoteApi
import time

def aproximar(C):
    for i in range(len(C)):
        for j in range(len(i)):
            if C[i,j]>0.7:
                C[i,j]=1
            else:
                C[i,j]=0
    return C
def suma(img):
    R=0
    G=0
    B=0
    for i in img:
        Red=i[0]
        Green=i[1]
        Blue=i[2]
        if (Red>200 and Green<50 and Blue<50):
            R=R+1
        if (Red<50 and Green>200 and Blue<50):
            G=G+1
        if (Red<50 and Green<50 and Blue>200):
            B=B+1
    return R,G,B
def bit2im(imagen_bit):
    n_pixeles=128*128
    pixeles=[]
    for i in range(n_pixeles):
        pixeles.append(imagen_bit[i*3:(((i+1)*3))])
    return pixeles
def color(sum):
    if max(sum)==0:
        colour=0
    elif sum[0]==max(sum):
        colour=1
    elif sum[1]==max(sum):
        colour=2
    elif sum[2]==max(sum):
        colour=3
    return colour

with b0RemoteApi.RemoteApiClient('b0RemoteApi_pythonClient','b0RemoteApi',60) as client:

    def callb(msg):
        print(msg)
    camera_R=client.simxGetObjectHandle("ePuck_camera", client.simxServiceCall())
    camera_R=camera_R[1]
    camera_G=client.simxGetObjectHandle("ePuck_camera#0", client.simxServiceCall())
    camera_G=camera_G[1]
    camera_B=client.simxGetObjectHandle("ePuck_camera#1", client.simxServiceCall())
    camera_B=camera_B[1]
    while True:
        imageR=client.simxGetVisionSensorImage(camera_R, False, client.simxServiceCall())
        imageR=imageR[2]
        imageR=bit2im(imageR)
        imageG=client.simxGetVisionSensorImage(camera_G, False, client.simxServiceCall())
        imageG=imageG[2]
        imageG=bit2im(imageG)
        imageB=client.simxGetVisionSensorImage(camera_B, False, client.simxServiceCall())
        imageB=imageB[2]
        imageB=bit2im(imageB)
        sumR=suma(imageR)
        sumG=suma(imageG)
        sumB=suma(imageB)
        client.simxSetIntSignal("Color_0", color(sumR), client.simxServiceCall())
        client.simxSetIntSignal("Color_1", color(sumG), client.simxServiceCall())
        client.simxSetIntSignal("Color_2", color(sumB), client.simxServiceCall())
