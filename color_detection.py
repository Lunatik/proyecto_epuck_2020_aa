import b0RemoteApi
import time
import cv2
import numpy as np
from PIL import Image
import array

def aproximar(C):
    for i in range(len(C)):
        for j in range(len(i)):
            if C[i,j]>0.7:
                C[i,j]=1
            else:
                C[i,j]=0
    return C
def suma2(img):
    R=0
    G=0
    B=0
    for i in range(128):
        for j in range(128):
            Red=img[i,j][0]
            Green=img[i,j][1]
            Blue=img[i,j][2]
            if (Red>100 and Green<50 and Blue<50):
                R=R+1
            if (Red<50 and Green>100 and Blue<50):
                G=G+1
            if (Red<50 and Green<50 and Blue>100):
                B=B+1
    return R,G,B
def suma(img):
    R=0
    G=0
    B=0
    centro=img[:,60:70,:]
    for j in centro:
        for i in j:
            Red=i[0]
            Green=i[1]
            Blue=i[2]
            if (Red>200 and Green<50 and Blue<50):
                R=R+1
            if (Red<50 and Green>200 and Blue<50):
                G=G+1
            if (Red<50 and Green<50 and Blue>200):
                B=B+1
    return R,G,B
def bit2im(imagen_bit):
    n_pixeles=128*128
    pixeles=[]
    image=np.zeros((128,128,3))
    for i in range(n_pixeles):
        pixeles.append(imagen_bit[i*3:(((i+1)*3))])
    index=0
    for fila in range(128):
        for columna in range(128):
            image[fila,columna,0]=int(pixeles[i][0])
            image[fila,columna,1]=int(pixeles[i][1])
            image[fila,columna,2]=int(pixeles[i][2])
            index=index+1
    return image
def buscar_color(ePuck_camera,client):
    camera=client.simxGetObjectHandle(ePuck_camera, client.simxServiceCall())
    camera=camera[1]
    image=client.simxGetVisionSensorImage(camera, False, client.simxServiceCall())
    image=image[2]
    image = Image.frombytes("RGB", (128,128), image, 'raw', "RGB", 0, 1)
    image= image.rotate(angle=180)
    image=image.load()
    sum=suma2(image)
    if max(sum)==0:
        color=0
    elif sum[0]==max(sum):
        color=1
    elif sum[1]==max(sum):
        color=2
    elif sum[2]==max(sum):
        color=3
    return color
def detect_line(ePuck_lightSensor,client):
    camera=client.simxGetObjectHandle(ePuck_lightSensor, client.simxServiceCall())
    camera=camera[1]
    image=client.simxGetVisionSensorImage(camera, True, client.simxServiceCall())
    image=image[2]
    sensor=image[64]
    value=0
    if sensor>200:
        value=1
    return value
with b0RemoteApi.RemoteApiClient('b0RemoteApi_pythonClient','b0RemoteApi',60) as client:
    while True:
        color_0=buscar_color("ePuck_camera",client)
        client.simxSetIntSignal("Color_0", color_0, client.simxServiceCall())
        color_1=buscar_color("ePuck_camera#0",client)
        client.simxSetIntSignal("Color_1", color_1, client.simxServiceCall())
        color_2=buscar_color("ePuck_camera#1",client)
        client.simxSetIntSignal("Color_2", color_2, client.simxServiceCall())
